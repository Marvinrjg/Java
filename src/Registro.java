
import java.awt.Image;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Registro extends javax.swing.JFrame {

    Connection con = null;
    Statement stmt = null;

    public Registro() {
        initComponents();
   setIconImage(new ImageIcon(getClass().getResource("/imagenes/1473.png")).getImage());
    
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lb_nombre = new javax.swing.JLabel();
        lb_contraseña = new javax.swing.JLabel();
        lb_nocuenta = new javax.swing.JLabel();
        txt_nombre = new javax.swing.JTextField();
        txt_nocuenta = new javax.swing.JTextField();
        cb_carrera = new javax.swing.JComboBox<>();
        btn_guardar = new javax.swing.JButton();
        btn_limpiar = new javax.swing.JButton();
        btn_iniciarsesion = new javax.swing.JButton();
        lb_fondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Universidad");
        setLocation(new java.awt.Point(320, 170));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lb_nombre.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        lb_nombre.setText("USUARIO:");
        getContentPane().add(lb_nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 50, 130, 40));

        lb_contraseña.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        lb_contraseña.setText("CARRERA:");
        getContentPane().add(lb_contraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 230, -1, -1));

        lb_nocuenta.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        lb_nocuenta.setText("NO. DE CUENTA:");
        getContentPane().add(lb_nocuenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 150, -1, -1));

        txt_nombre.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txt_nombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nombreActionPerformed(evt);
            }
        });
        getContentPane().add(txt_nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 50, 250, 30));

        txt_nocuenta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txt_nocuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nocuentaActionPerformed(evt);
            }
        });
        getContentPane().add(txt_nocuenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 150, 170, 30));

        cb_carrera.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        cb_carrera.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SELECCIONE", "INGENIERIA EN COMPUTACIÓN" }));
        cb_carrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_carreraActionPerformed(evt);
            }
        });
        getContentPane().add(cb_carrera, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 230, 230, 30));

        btn_guardar.setBackground(new java.awt.Color(153, 153, 153));
        btn_guardar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_guardar.setText("GUARDAR");
        btn_guardar.setMaximumSize(new java.awt.Dimension(100, 100));
        btn_guardar.setMinimumSize(new java.awt.Dimension(100, 100));
        btn_guardar.setPreferredSize(new java.awt.Dimension(100, 100));
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });
        getContentPane().add(btn_guardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 310, 110, 40));

        btn_limpiar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_limpiar.setText("LIMPIAR");
        btn_limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_limpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btn_limpiar, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 90, 110, 40));

        btn_iniciarsesion.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btn_iniciarsesion.setText("INICIAR SESIÓN");
        btn_iniciarsesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_iniciarsesionActionPerformed(evt);
            }
        });
        getContentPane().add(btn_iniciarsesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 180, 150, 40));

        lb_fondo.setFont(new java.awt.Font("Sitka Heading", 1, 36)); // NOI18N
        lb_fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondo-azul-exagonos-fotorecurso.jpg"))); // NOI18N
        getContentPane().add(lb_fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 680, 400));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_nombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_nombreActionPerformed

    private void txt_nocuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nocuentaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_nocuentaActionPerformed

    private void cb_carreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_carreraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb_carreraActionPerformed

    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed

        String nombrecompleto, nocuenta, carrera;

        nombrecompleto = txt_nombre.getText();
        nocuenta = txt_nocuenta.getText();
        carrera = cb_carrera.getSelectedItem().toString();
       

        if ((txt_nombre.getText().equals("") || txt_nocuenta.getText().equals("")) || (cb_carrera.getSelectedItem().equals("SELECCIONE"))) {

            //javax.swing.JOptionPane.showMessageDialog(this, "Debe llenar todos los campos \n", "AVISO!", javax.swing.JOptionPane.INFORMATION_MESSAGE);
            JOptionPane.showMessageDialog(this, "Debes llenar todos los datos prro", "Aviso", 1);
            txt_nombre.requestFocus();
        } else {
            try {

                String url = "jdbc:mysql://localhost:3306/universidadpro";
                String usuario = "root";
                String contraseña = "";

                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con = DriverManager.getConnection(url, usuario, contraseña);
                if (con != null) {
                    System.out.println("Se ha establecido una conexión a la base de datos "
                            + "\n " + url);
                }
                
                stmt = con.createStatement();
                stmt.executeUpdate("INSERT INTO proyectopersonal VALUES('" + 0 + "','" + nombrecompleto + "','" + nocuenta + "','" + carrera + "')");
                System.out.println("Los valores han sido agregados a la base de datos ");

            } catch (InstantiationException ex) {
                Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (con != null) {
                    try {
                        con.close();
                        stmt.close();
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
            javax.swing.JOptionPane.showMessageDialog(this, "Registro exitoso! \n", "AVISO!", javax.swing.JOptionPane.INFORMATION_MESSAGE);
        }
        this.txt_nombre.setText("");
        this.txt_nocuenta.setText("");
        this.cb_carrera.setSelectedIndex(0);
       
        
        


    }//GEN-LAST:event_btn_guardarActionPerformed

    
    
    
    private void btn_limpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_limpiarActionPerformed
        txt_nombre.setText("");
        txt_nocuenta.setText("");
        cb_carrera.setSelectedIndex(0);
      
    }//GEN-LAST:event_btn_limpiarActionPerformed

    private void btn_iniciarsesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_iniciarsesionActionPerformed
       this.dispose();
        InicioSesion in = new InicioSesion();
       in.setVisible(true);
       
    }//GEN-LAST:event_btn_iniciarsesionActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Registro().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_guardar;
    private javax.swing.JButton btn_iniciarsesion;
    private javax.swing.JButton btn_limpiar;
    private javax.swing.JComboBox<String> cb_carrera;
    private javax.swing.JLabel lb_contraseña;
    private javax.swing.JLabel lb_fondo;
    private javax.swing.JLabel lb_nocuenta;
    private javax.swing.JLabel lb_nombre;
    private javax.swing.JTextField txt_nocuenta;
    private javax.swing.JTextField txt_nombre;
    // End of variables declaration//GEN-END:variables
}
