
import java.awt.Image;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class InicioSesion extends javax.swing.JFrame {

    Connection con = null;
    Statement stmt = null;

    public InicioSesion() {
        initComponents();
      
  setIconImage(new ImageIcon(getClass().getResource("/imagenes/1473.png")).getImage());
       
   /*     
try{
    Image img = ImageIO.read(new File("1473.png"));
    this.setIconImage(img);
}catch (Exception e){
    System.out.println(e);
}*/
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lb_usuarioinicio = new javax.swing.JLabel();
        lb_contrasenainicio = new javax.swing.JLabel();
        txt_nombre = new javax.swing.JTextField();
        btn_iniciars = new javax.swing.JButton();
        btn_registrar = new javax.swing.JButton();
        txt_nocuenta = new javax.swing.JTextField();
        lb_fondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocation(new java.awt.Point(450, 250));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lb_usuarioinicio.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_usuarioinicio.setForeground(new java.awt.Color(255, 255, 255));
        lb_usuarioinicio.setText("USUARIO:");
        getContentPane().add(lb_usuarioinicio, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 40, -1, -1));

        lb_contrasenainicio.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_contrasenainicio.setForeground(new java.awt.Color(255, 255, 255));
        lb_contrasenainicio.setText("NO. CUENTA:");
        getContentPane().add(lb_contrasenainicio, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 90, -1, -1));

        txt_nombre.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txt_nombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nombreActionPerformed(evt);
            }
        });
        getContentPane().add(txt_nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 40, 210, -1));

        btn_iniciars.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btn_iniciars.setText("ENTRAR");
        btn_iniciars.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_iniciarsActionPerformed(evt);
            }
        });
        getContentPane().add(btn_iniciars, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 140, 120, 40));

        btn_registrar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btn_registrar.setText("REGISTRARSE");
        btn_registrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_registrarActionPerformed(evt);
            }
        });
        getContentPane().add(btn_registrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 140, 120, 40));

        txt_nocuenta.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        getContentPane().add(txt_nocuenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 90, 170, -1));

        lb_fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/wpdlphp.jpg"))); // NOI18N
        getContentPane().add(lb_fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 430, 210));

        pack();
    }// </editor-fold>//GEN-END:initComponents

 
    
    
    private void txt_nombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_nombreActionPerformed

    void ingresar(String user, String clave) {

    }

    private void btn_iniciarsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_iniciarsActionPerformed

        //DefaultTableModel modelo;
        String nombre = txt_nombre.getText();
        String nocuenta = txt_nocuenta.getText();
        //ingresar(nocuenta,pass);
        //Panel pn = new Panel();
        //pn.setVisible(true);
        int conta = 0;

        this.setTitle("Iniciar Sesión");
        //this.setLocation(335, 200);
        //this.setResizable(false);
       

        try {

            String url = "jdbc:mysql://localhost:3306/universidadpro";
            String usuario = "root";
            String contraseña = "";

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con = DriverManager.getConnection(url, usuario, contraseña);
            if (con != null) {
                System.out.println("Se ha establecido una conexion a la base de datos" + "\n" + url);
            }

            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM proyectopersonal WHERE nombrecompleto = '"+ nombre +"' && nocuenta = '"+ nocuenta+"'");

            for(int i=1; i<=1;i++){
          if(txt_nocuenta.equals("") || txt_nombre.getText().equals("")){
              JOptionPane.showMessageDialog(this, "Complete todos los campos", "Campos vacios", 1);
          }}
             
            while(rs.next()){
                int idus = rs.getInt("idusuarios");
                String nomb = rs.getString("nombrecompleto");
                String nocue = rs.getString("nocuenta");
                
                   if(!nombre.equals(nomb) || !nocuenta.equals(nocue)){
                    //JOptionPane.showMessageDialog(this, "BIENVENIDO " + nombre);
                     JOptionPane.showMessageDialog(this, "Usuario y/o contraseña incorrectos prro" );
                   // Panel pn = new Panel();
                   // pn.setVisible(true);
                   // this.dispose();
                    //break;
                
                   
                }
                
                else {
                   // conta++; 
                        JOptionPane.showMessageDialog(this, "Bienvenido " + nombre);
               Panel pn = new Panel();
                    pn.setVisible(true);
                    this.dispose();
                    break;
                     //if(!nombre.equals(nomb)|| !nocuenta.equals(nocue)){
                      // JOptionPane.showMessageDialog(this, "Usuario y/o contraseña incorrectos prro" );
                //} 
                }
                  
                   
               // if(conta > idus){
                      // JOptionPane.showMessageDialog(this, "Usuario y/o contraseña incorrectos prro" );
                //}
            }
            
             
            
            
        } catch (Exception e) {

            JOptionPane.showMessageDialog(this, "No está conectado a la base de datos");
        }


    }//GEN-LAST:event_btn_iniciarsActionPerformed

    
    
    private void btn_registrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_registrarActionPerformed
        this.dispose();
        Registro rg = new Registro();
        rg.setVisible(true);
       
    }//GEN-LAST:event_btn_registrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InicioSesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InicioSesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InicioSesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InicioSesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InicioSesion().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_iniciars;
    private javax.swing.JButton btn_registrar;
    private javax.swing.JLabel lb_contrasenainicio;
    private javax.swing.JLabel lb_fondo;
    private javax.swing.JLabel lb_usuarioinicio;
    private javax.swing.JTextField txt_nocuenta;
    private javax.swing.JTextField txt_nombre;
    // End of variables declaration//GEN-END:variables
}
