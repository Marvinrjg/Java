
import java.awt.Image;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Calificaciones extends javax.swing.JFrame {

    Connection con = null;
    Statement stmt = null;
    String var, var2;

    public Calificaciones() {
        initComponents();
        
   setIconImage(new ImageIcon(getClass().getResource("/imagenes/1473.png")).getImage());
    
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        lb_nocuenta = new javax.swing.JLabel();
        txt_nocuenta = new javax.swing.JTextField();
        cb_semestre = new javax.swing.JComboBox<>();
        btn_consultar = new javax.swing.JButton();
        lb_materia1 = new javax.swing.JLabel();
        lb_materia2 = new javax.swing.JLabel();
        lb_materia3 = new javax.swing.JLabel();
        lb_materia4 = new javax.swing.JLabel();
        lb_materia6 = new javax.swing.JLabel();
        lb_materia5 = new javax.swing.JLabel();
        lb_materia7 = new javax.swing.JLabel();
        lb_materia8 = new javax.swing.JLabel();
        lb_optativa1 = new javax.swing.JLabel();
        lb_optativa2 = new javax.swing.JLabel();
        lb_parcial1_mat = new javax.swing.JLabel();
        lb_parcial2_mat = new javax.swing.JLabel();
        lb_parcial3_mat = new javax.swing.JLabel();
        lb_final_mat = new javax.swing.JLabel();
        txt_materia1 = new javax.swing.JTextField();
        txt_materia2 = new javax.swing.JTextField();
        txt_materia5 = new javax.swing.JTextField();
        txt_materia6 = new javax.swing.JTextField();
        txt_materia7 = new javax.swing.JTextField();
        txt_materia8 = new javax.swing.JTextField();
        txt_m1parcial2 = new javax.swing.JTextField();
        txt_m1parcial3 = new javax.swing.JTextField();
        txt_m1parcial1 = new javax.swing.JTextField();
        txt_m1final = new javax.swing.JTextField();
        txt_m2parcial2 = new javax.swing.JTextField();
        txt_m2parcial3 = new javax.swing.JTextField();
        txt_m2parcial1 = new javax.swing.JTextField();
        txt_m2final = new javax.swing.JTextField();
        txt_m3parcial2 = new javax.swing.JTextField();
        txt_m3parcial3 = new javax.swing.JTextField();
        txt_m3parcial1 = new javax.swing.JTextField();
        txt_m3final = new javax.swing.JTextField();
        txt_m4parcial2 = new javax.swing.JTextField();
        txt_m4parcial3 = new javax.swing.JTextField();
        txt_m4parcial1 = new javax.swing.JTextField();
        txt_m4final = new javax.swing.JTextField();
        txt_m5parcial2 = new javax.swing.JTextField();
        txt_m5parcial3 = new javax.swing.JTextField();
        txt_m5parcial1 = new javax.swing.JTextField();
        txt_m5final = new javax.swing.JTextField();
        txt_m6parcial2 = new javax.swing.JTextField();
        txt_m6parcial3 = new javax.swing.JTextField();
        txt_m6parcial1 = new javax.swing.JTextField();
        txt_m6final = new javax.swing.JTextField();
        txt_m7parcial2 = new javax.swing.JTextField();
        txt_m7parcial3 = new javax.swing.JTextField();
        txt_m7parcial1 = new javax.swing.JTextField();
        txt_m7final = new javax.swing.JTextField();
        txt_m8parcial2 = new javax.swing.JTextField();
        txt_m8parcial3 = new javax.swing.JTextField();
        txt_m8parcial1 = new javax.swing.JTextField();
        txt_m8final = new javax.swing.JTextField();
        txt_o1parcial2 = new javax.swing.JTextField();
        txt_o1parcial3 = new javax.swing.JTextField();
        txt_o1parcial1 = new javax.swing.JTextField();
        txt_o1final = new javax.swing.JTextField();
        txt_o2parcial2 = new javax.swing.JTextField();
        txt_o2parcial3 = new javax.swing.JTextField();
        txt_o2parcial1 = new javax.swing.JTextField();
        txt_o2final = new javax.swing.JTextField();
        txt_optativa1 = new javax.swing.JTextField();
        txt_optativa2 = new javax.swing.JTextField();
        txt_materia3 = new javax.swing.JTextField();
        txt_materia4 = new javax.swing.JTextField();
        btn_regresar = new javax.swing.JButton();
        btn_promedio = new javax.swing.JButton();
        lb_promsem = new javax.swing.JLabel();
        txt_promsem = new javax.swing.JTextField();
        txt_id = new javax.swing.JTextField();
        lb_id = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/depositphotos_3750383-stock-photo-crushed-grunge-paper-background.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lb_nocuenta.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_nocuenta.setForeground(new java.awt.Color(255, 255, 255));
        lb_nocuenta.setText("No.Cuenta:");
        getContentPane().add(lb_nocuenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, -1, -1));

        txt_nocuenta.setBackground(new java.awt.Color(153, 102, 0));
        txt_nocuenta.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        txt_nocuenta.setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().add(txt_nocuenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 70, 100, -1));

        cb_semestre.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        cb_semestre.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione", "Semestre 1", "Semestre 2", "Semestre 3", "Semestre 4", "Semestre 5", "Semestre 6", "Semestre 7", "Semestre 8", "Semestre 9" }));
        cb_semestre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_semestreActionPerformed(evt);
            }
        });
        getContentPane().add(cb_semestre, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 60, -1, 40));

        btn_consultar.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        btn_consultar.setText("Consultar");
        btn_consultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_consultarActionPerformed(evt);
            }
        });
        getContentPane().add(btn_consultar, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 60, -1, 30));

        lb_materia1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia1.setForeground(new java.awt.Color(255, 255, 255));
        lb_materia1.setText("Materia 1:");
        getContentPane().add(lb_materia1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, -1, -1));

        lb_materia2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia2.setForeground(new java.awt.Color(255, 255, 255));
        lb_materia2.setText("Materia 2:");
        getContentPane().add(lb_materia2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, -1, -1));

        lb_materia3.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia3.setForeground(new java.awt.Color(255, 255, 255));
        lb_materia3.setText("Materia 3:");
        getContentPane().add(lb_materia3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, -1, -1));

        lb_materia4.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia4.setForeground(new java.awt.Color(255, 255, 255));
        lb_materia4.setText("Materia 4:");
        getContentPane().add(lb_materia4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 260, -1, -1));

        lb_materia6.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia6.setForeground(new java.awt.Color(255, 255, 255));
        lb_materia6.setText("Materia 6:");
        getContentPane().add(lb_materia6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 340, -1, -1));

        lb_materia5.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia5.setForeground(new java.awt.Color(255, 255, 255));
        lb_materia5.setText("Materia 5:");
        getContentPane().add(lb_materia5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 300, -1, -1));

        lb_materia7.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia7.setForeground(new java.awt.Color(255, 255, 255));
        lb_materia7.setText("Materia 7:");
        getContentPane().add(lb_materia7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 380, -1, -1));

        lb_materia8.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia8.setForeground(new java.awt.Color(255, 255, 255));
        lb_materia8.setText("Materia 8:");
        getContentPane().add(lb_materia8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 420, -1, -1));

        lb_optativa1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_optativa1.setForeground(new java.awt.Color(255, 255, 255));
        lb_optativa1.setText("Optativa 1:");
        getContentPane().add(lb_optativa1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 460, -1, -1));

        lb_optativa2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_optativa2.setForeground(new java.awt.Color(255, 255, 255));
        lb_optativa2.setText("Optativa 2:");
        getContentPane().add(lb_optativa2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 500, -1, -1));

        lb_parcial1_mat.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_parcial1_mat.setForeground(new java.awt.Color(255, 255, 255));
        lb_parcial1_mat.setText("Parcial 1");
        getContentPane().add(lb_parcial1_mat, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 100, -1, -1));

        lb_parcial2_mat.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_parcial2_mat.setForeground(new java.awt.Color(255, 255, 255));
        lb_parcial2_mat.setText("Parcial 2");
        getContentPane().add(lb_parcial2_mat, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 100, -1, -1));

        lb_parcial3_mat.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_parcial3_mat.setForeground(new java.awt.Color(255, 255, 255));
        lb_parcial3_mat.setText("Parcial 3");
        getContentPane().add(lb_parcial3_mat, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 100, -1, -1));

        lb_final_mat.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_final_mat.setForeground(new java.awt.Color(255, 255, 255));
        lb_final_mat.setText("Final");
        getContentPane().add(lb_final_mat, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 100, -1, -1));

        txt_materia1.setBackground(new java.awt.Color(204, 204, 204));
        txt_materia1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        txt_materia1.setAlignmentX(1.0F);
        txt_materia1.setAlignmentY(1.0F);
        txt_materia1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_materia1ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_materia1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 140, 260, -1));

        txt_materia2.setBackground(new java.awt.Color(204, 204, 204));
        txt_materia2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        txt_materia2.setAlignmentX(1.0F);
        txt_materia2.setAlignmentY(1.0F);
        txt_materia2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_materia2ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_materia2, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 180, 260, -1));

        txt_materia5.setBackground(new java.awt.Color(204, 204, 204));
        txt_materia5.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        txt_materia5.setAlignmentX(1.0F);
        txt_materia5.setAlignmentY(1.0F);
        txt_materia5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_materia5ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_materia5, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 300, 260, -1));

        txt_materia6.setBackground(new java.awt.Color(204, 204, 204));
        txt_materia6.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        txt_materia6.setAlignmentX(1.0F);
        txt_materia6.setAlignmentY(1.0F);
        txt_materia6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_materia6ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_materia6, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 340, 260, -1));

        txt_materia7.setBackground(new java.awt.Color(204, 204, 204));
        txt_materia7.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        txt_materia7.setAlignmentX(1.0F);
        txt_materia7.setAlignmentY(1.0F);
        txt_materia7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_materia7ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_materia7, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 380, 260, -1));

        txt_materia8.setBackground(new java.awt.Color(204, 204, 204));
        txt_materia8.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        txt_materia8.setAlignmentX(1.0F);
        txt_materia8.setAlignmentY(1.0F);
        txt_materia8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_materia8ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_materia8, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 420, 260, -1));

        txt_m1parcial2.setBackground(new java.awt.Color(204, 204, 204));
        txt_m1parcial2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m1parcial2, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 140, 30, -1));

        txt_m1parcial3.setBackground(new java.awt.Color(204, 204, 204));
        txt_m1parcial3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m1parcial3, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 140, 30, -1));

        txt_m1parcial1.setBackground(new java.awt.Color(204, 204, 204));
        txt_m1parcial1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m1parcial1, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 140, 30, -1));

        txt_m1final.setBackground(new java.awt.Color(204, 204, 204));
        txt_m1final.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m1final, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 140, 120, -1));

        txt_m2parcial2.setBackground(new java.awt.Color(204, 204, 204));
        txt_m2parcial2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m2parcial2, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 180, 30, -1));

        txt_m2parcial3.setBackground(new java.awt.Color(204, 204, 204));
        txt_m2parcial3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m2parcial3, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 180, 30, -1));

        txt_m2parcial1.setBackground(new java.awt.Color(204, 204, 204));
        txt_m2parcial1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m2parcial1, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 180, 30, -1));

        txt_m2final.setBackground(new java.awt.Color(204, 204, 204));
        txt_m2final.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m2final, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 180, 120, -1));

        txt_m3parcial2.setBackground(new java.awt.Color(204, 204, 204));
        txt_m3parcial2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m3parcial2, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 220, 30, -1));

        txt_m3parcial3.setBackground(new java.awt.Color(204, 204, 204));
        txt_m3parcial3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m3parcial3, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 220, 30, -1));

        txt_m3parcial1.setBackground(new java.awt.Color(204, 204, 204));
        txt_m3parcial1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m3parcial1, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 220, 30, -1));

        txt_m3final.setBackground(new java.awt.Color(204, 204, 204));
        txt_m3final.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m3final, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 220, 120, -1));

        txt_m4parcial2.setBackground(new java.awt.Color(204, 204, 204));
        txt_m4parcial2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m4parcial2, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 260, 30, -1));

        txt_m4parcial3.setBackground(new java.awt.Color(204, 204, 204));
        txt_m4parcial3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m4parcial3, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 260, 30, -1));

        txt_m4parcial1.setBackground(new java.awt.Color(204, 204, 204));
        txt_m4parcial1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m4parcial1, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 260, 30, -1));

        txt_m4final.setBackground(new java.awt.Color(204, 204, 204));
        txt_m4final.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m4final, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 260, 120, -1));

        txt_m5parcial2.setBackground(new java.awt.Color(204, 204, 204));
        txt_m5parcial2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m5parcial2, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 300, 30, -1));

        txt_m5parcial3.setBackground(new java.awt.Color(204, 204, 204));
        txt_m5parcial3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m5parcial3, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 300, 30, -1));

        txt_m5parcial1.setBackground(new java.awt.Color(204, 204, 204));
        txt_m5parcial1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m5parcial1, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 300, 30, -1));

        txt_m5final.setBackground(new java.awt.Color(204, 204, 204));
        txt_m5final.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m5final, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 300, 120, -1));

        txt_m6parcial2.setBackground(new java.awt.Color(204, 204, 204));
        txt_m6parcial2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m6parcial2, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 340, 30, -1));

        txt_m6parcial3.setBackground(new java.awt.Color(204, 204, 204));
        txt_m6parcial3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m6parcial3, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 340, 30, -1));

        txt_m6parcial1.setBackground(new java.awt.Color(204, 204, 204));
        txt_m6parcial1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m6parcial1, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 340, 30, -1));

        txt_m6final.setBackground(new java.awt.Color(204, 204, 204));
        txt_m6final.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m6final, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 340, 120, -1));

        txt_m7parcial2.setBackground(new java.awt.Color(204, 204, 204));
        txt_m7parcial2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m7parcial2, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 380, 30, -1));

        txt_m7parcial3.setBackground(new java.awt.Color(204, 204, 204));
        txt_m7parcial3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m7parcial3, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 380, 30, -1));

        txt_m7parcial1.setBackground(new java.awt.Color(204, 204, 204));
        txt_m7parcial1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m7parcial1, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 380, 30, -1));

        txt_m7final.setBackground(new java.awt.Color(204, 204, 204));
        txt_m7final.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m7final, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 380, 120, -1));

        txt_m8parcial2.setBackground(new java.awt.Color(204, 204, 204));
        txt_m8parcial2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m8parcial2, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 420, 30, -1));

        txt_m8parcial3.setBackground(new java.awt.Color(204, 204, 204));
        txt_m8parcial3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m8parcial3, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 420, 30, -1));

        txt_m8parcial1.setBackground(new java.awt.Color(204, 204, 204));
        txt_m8parcial1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m8parcial1, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 420, 30, -1));

        txt_m8final.setBackground(new java.awt.Color(204, 204, 204));
        txt_m8final.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_m8final, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 420, 120, -1));

        txt_o1parcial2.setBackground(new java.awt.Color(204, 204, 204));
        txt_o1parcial2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_o1parcial2, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 460, 30, -1));

        txt_o1parcial3.setBackground(new java.awt.Color(204, 204, 204));
        txt_o1parcial3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_o1parcial3, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 460, 30, -1));

        txt_o1parcial1.setBackground(new java.awt.Color(204, 204, 204));
        txt_o1parcial1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_o1parcial1, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 460, 30, -1));

        txt_o1final.setBackground(new java.awt.Color(204, 204, 204));
        txt_o1final.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_o1final, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 460, 120, -1));

        txt_o2parcial2.setBackground(new java.awt.Color(204, 204, 204));
        txt_o2parcial2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_o2parcial2, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 500, 30, -1));

        txt_o2parcial3.setBackground(new java.awt.Color(204, 204, 204));
        txt_o2parcial3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_o2parcial3, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 500, 30, -1));

        txt_o2parcial1.setBackground(new java.awt.Color(204, 204, 204));
        txt_o2parcial1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_o2parcial1, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 500, 30, -1));

        txt_o2final.setBackground(new java.awt.Color(204, 204, 204));
        txt_o2final.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        getContentPane().add(txt_o2final, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 500, 120, -1));

        txt_optativa1.setBackground(new java.awt.Color(204, 204, 204));
        txt_optativa1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        txt_optativa1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_optativa1ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_optativa1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 460, 260, -1));

        txt_optativa2.setBackground(new java.awt.Color(204, 204, 204));
        txt_optativa2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        txt_optativa2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_optativa2ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_optativa2, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 500, 260, -1));

        txt_materia3.setBackground(new java.awt.Color(204, 204, 204));
        txt_materia3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        txt_materia3.setAlignmentX(1.0F);
        txt_materia3.setAlignmentY(1.0F);
        txt_materia3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_materia3ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_materia3, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 220, 260, -1));

        txt_materia4.setBackground(new java.awt.Color(204, 204, 204));
        txt_materia4.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        txt_materia4.setAlignmentX(1.0F);
        txt_materia4.setAlignmentY(1.0F);
        txt_materia4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_materia4ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_materia4, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 260, 260, -1));

        btn_regresar.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        btn_regresar.setText("Registro de Calif.");
        btn_regresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_regresarActionPerformed(evt);
            }
        });
        getContentPane().add(btn_regresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 60, -1, -1));

        btn_promedio.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        btn_promedio.setText("Promedio");
        btn_promedio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_promedioActionPerformed(evt);
            }
        });
        getContentPane().add(btn_promedio, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 200, 110, 70));

        lb_promsem.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_promsem.setForeground(new java.awt.Color(255, 255, 255));
        lb_promsem.setText("Promedio Semestre");
        getContentPane().add(lb_promsem, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 310, 190, 40));

        txt_promsem.setBackground(new java.awt.Color(153, 102, 0));
        txt_promsem.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        txt_promsem.setForeground(new java.awt.Color(255, 255, 255));
        txt_promsem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_promsemActionPerformed(evt);
            }
        });
        getContentPane().add(txt_promsem, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 360, 250, 70));

        txt_id.setBackground(new java.awt.Color(153, 153, 153));
        txt_id.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_idActionPerformed(evt);
            }
        });
        getContentPane().add(txt_id, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, 20, 20));

        lb_id.setText("ID");
        getContentPane().add(lb_id, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 20, 20));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/depositphotos_3750383-stock-photo-crushed-grunge-paper-background.jpg"))); // NOI18N
        jLabel2.setText("jLabel2");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1020, 590));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_materia1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_materia1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_materia1ActionPerformed

    private void txt_materia2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_materia2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_materia2ActionPerformed

    private void txt_materia3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_materia3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_materia3ActionPerformed

    private void txt_materia4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_materia4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_materia4ActionPerformed

    private void txt_materia5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_materia5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_materia5ActionPerformed

    private void txt_materia6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_materia6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_materia6ActionPerformed

    private void txt_materia7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_materia7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_materia7ActionPerformed

    private void txt_materia8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_materia8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_materia8ActionPerformed

    private void txt_optativa1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_optativa1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_optativa1ActionPerformed

    private void txt_optativa2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_optativa2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_optativa2ActionPerformed

    private void cb_semestreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_semestreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb_semestreActionPerformed

    private void btn_consultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_consultarActionPerformed
        String cap = "";
        ResultSet rs = null;
        var = txt_nocuenta.getText() + "_" + cb_semestre.getSelectedItem(); //javax.swing.JOptionPane.showInputDialog(this,"Nombre del usuario","Consulta usuario",javax.swing.JOptionPane.QUESTION_MESSAGE);
        String sql = "SELECT* FROM materias WHERE nocuenta = '" + var + "'";
        if (var == null) {
            javax.swing.JOptionPane.showMessageDialog(this, "La accion fue cancelada", "AVISO!", javax.swing.JOptionPane.INFORMATION_MESSAGE);
        } else {
            if (var.equals("") || txt_nocuenta.getText().equals("")) {
                javax.swing.JOptionPane.showMessageDialog(this, "Favor de ingresar el numero de cuenta que desea consultar", "AVISO!", javax.swing.JOptionPane.INFORMATION_MESSAGE);
            } if(cb_semestre.getSelectedItem().equals("Seleccione")){
                JOptionPane.showMessageDialog(this, "Selecciona el semestre");
            }else {
                try {

                    String url = "jdbc:mysql://localhost:3306/universidadpro";
                    String usuario = "root";
                    String contraseña = "";

                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                    con = DriverManager.getConnection(url, usuario, contraseña);
                    if (con != null) {
                        System.out.println("Se ha establecido una conexión a la base de datos "
                                + "\n " + url);
                    }

                    stmt = con.createStatement();
                    rs = stmt.executeQuery(sql);

                    //String materia1 =  rs.getString("materia1");
                    //txt_materia1.setText(materia1); 
                    //while(rs.next()) {
                    //cap = rs.getString("tipouser"); 
                    //if (cap.equals("INVITADO") || cap.equals("ADMINISTRADOR")) {
                    consulta();
                    //}
                    //}   // fin del bucle While

                } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(Calificaciones.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                            stmt.close();
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }
                    }
                }

            }
        }
    }//GEN-LAST:event_btn_consultarActionPerformed

    private void btn_regresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_regresarActionPerformed
        Panel pn = new Panel();
        pn.setVisible(true);
        this.dispose();

    }//GEN-LAST:event_btn_regresarActionPerformed

    private void btn_promedioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_promedioActionPerformed
        double m1parcial1, m1parcial2, m1parcial3, finalmat1;

        m1parcial1 = Double.parseDouble(this.txt_m1parcial1.getText());
        m1parcial2 = Double.parseDouble(this.txt_m1parcial2.getText());
        m1parcial3 = Double.parseDouble(this.txt_m1parcial3.getText());

        finalmat1 = (m1parcial1 + m1parcial2 + m1parcial3) / (3);

        txt_m1final.setText(String.valueOf(finalmat1));

        double m2parcial1, m2parcial2, m2parcial3, finalmat2;

        m2parcial1 = Double.parseDouble(this.txt_m2parcial1.getText());
        m2parcial2 = Double.parseDouble(this.txt_m2parcial2.getText());
        m2parcial3 = Double.parseDouble(this.txt_m2parcial3.getText());

        finalmat2 = (m2parcial1 + m2parcial2 + m2parcial3) / (3);

        txt_m2final.setText(String.valueOf(finalmat2));

        double m3parcial1, m3parcial2, m3parcial3, finalmat3;

        m3parcial1 = Double.parseDouble(this.txt_m3parcial1.getText());
        m3parcial2 = Double.parseDouble(this.txt_m3parcial2.getText());
        m3parcial3 = Double.parseDouble(this.txt_m3parcial3.getText());

        finalmat3 = (m3parcial1 + m3parcial2 + m3parcial3) / (3);

        txt_m3final.setText(String.valueOf(finalmat3));

        double m4parcial1, m4parcial2, m4parcial3, finalmat4;

        m4parcial1 = Double.parseDouble(this.txt_m4parcial1.getText());
        m4parcial2 = Double.parseDouble(this.txt_m4parcial2.getText());
        m4parcial3 = Double.parseDouble(this.txt_m4parcial3.getText());

        finalmat4 = (m4parcial1 + m4parcial2 + m4parcial3) / (3);

        txt_m4final.setText(String.valueOf(finalmat4));

        double m5parcial1, m5parcial2, m5parcial3, finalmat5;

        m5parcial1 = Double.parseDouble(this.txt_m5parcial1.getText());
        m5parcial2 = Double.parseDouble(this.txt_m5parcial2.getText());
        m5parcial3 = Double.parseDouble(this.txt_m5parcial3.getText());

        finalmat5 = (m5parcial1 + m5parcial2 + m5parcial3) / (3);

        txt_m5final.setText(String.valueOf(finalmat5));

        double m6parcial1, m6parcial2, m6parcial3, finalmat6;

        m6parcial1 = Double.parseDouble(this.txt_m6parcial1.getText());
        m6parcial2 = Double.parseDouble(this.txt_m6parcial2.getText());
        m6parcial3 = Double.parseDouble(this.txt_m6parcial3.getText());

        finalmat6 = (m6parcial1 + m6parcial2 + m6parcial3) / (3);

        txt_m6final.setText(String.valueOf(finalmat6));

        double m7parcial1, m7parcial2, m7parcial3, finalmat7;

        m7parcial1 = Double.parseDouble(this.txt_m7parcial1.getText());
        m7parcial2 = Double.parseDouble(this.txt_m7parcial2.getText());
        m7parcial3 = Double.parseDouble(this.txt_m7parcial3.getText());

        finalmat7 = (m7parcial1 + m7parcial2 + m7parcial3) / (3);

        txt_m7final.setText(String.valueOf(finalmat7));

        double m8parcial1, m8parcial2, m8parcial3, finalmat8;

        m8parcial1 = Double.parseDouble(this.txt_m8parcial1.getText());
        m8parcial2 = Double.parseDouble(this.txt_m8parcial2.getText());
        m8parcial3 = Double.parseDouble(this.txt_m8parcial3.getText());

        finalmat8 = (m8parcial1 + m8parcial2 + m8parcial3) / (3);

        txt_m8final.setText(String.valueOf(finalmat8));

        double o1parcial1, o1parcial2, o1parcial3, finalo1;

        o1parcial1 = Double.parseDouble(this.txt_o1parcial1.getText());
        o1parcial2 = Double.parseDouble(this.txt_o1parcial2.getText());
        o1parcial3 = Double.parseDouble(this.txt_o1parcial3.getText());

        finalo1 = (o1parcial1 + o1parcial2 + o1parcial3) / (3);

        txt_o1final.setText(String.valueOf(finalo1));

        double o2parcial1, o2parcial2, o2parcial3, finalo2;

        o2parcial1 = Double.parseDouble(this.txt_o2parcial1.getText());
        o2parcial2 = Double.parseDouble(this.txt_o2parcial2.getText());
        o2parcial3 = Double.parseDouble(this.txt_o2parcial3.getText());

        finalo2 = (o2parcial1 + o2parcial2 + o2parcial3) / (3);

        txt_o2final.setText(String.valueOf(finalo2));

        if (!txt_materia1.getText().equals("Sin materia") && !txt_materia2.getText().equals("Sin materia") && !txt_materia3.getText().equals("Sin materia") && !txt_materia4.getText().equals("Sin materia") && !txt_materia5.getText().equals("Sin materia") && txt_materia6.getText().equals("Sin materia") && txt_materia7.getText().equals("Sin materia") && txt_materia8.getText().equals("Sin materia") && txt_optativa1.getText().equals("Sin materia") && txt_optativa2.getText().equals("Sin materia")) {
            double sumapromfin, promfin;
            sumapromfin = Double.parseDouble(txt_m1final.getText()) + Double.parseDouble(txt_m2final.getText()) + Double.parseDouble(txt_m3final.getText()) + Double.parseDouble(txt_m4final.getText()) + Double.parseDouble(txt_m5final.getText());
            promfin = sumapromfin / 5;
            txt_promsem.setText(String.valueOf(promfin));
            guardarPromedio(String.valueOf(promfin));
        } else if (!txt_materia1.getText().equals("Sin materia") && !txt_materia2.getText().equals("Sin materia") && !txt_materia3.getText().equals("Sin materia") && !txt_materia4.getText().equals("Sin materia") && !txt_materia5.getText().equals("Sin materia") && !txt_materia6.getText().equals("Sin materia") && txt_materia7.getText().equals("Sin materia") && txt_materia8.getText().equals("Sin materia") && txt_optativa1.getText().equals("Sin materia") && txt_optativa2.getText().equals("Sin materia")) {
            double sumapromfin, promfin;
            sumapromfin = Double.parseDouble(txt_m1final.getText()) + Double.parseDouble(txt_m2final.getText()) + Double.parseDouble(txt_m3final.getText()) + Double.parseDouble(txt_m4final.getText()) + Double.parseDouble(txt_m5final.getText()) + Double.parseDouble(txt_m6final.getText());
            promfin = sumapromfin / 6;
            txt_promsem.setText(String.valueOf(promfin));
            guardarPromedio(String.valueOf(promfin));
        } else if (!txt_materia1.getText().equals("Sin materia") && !txt_materia2.getText().equals("Sin materia") && !txt_materia3.getText().equals("Sin materia") && !txt_materia4.getText().equals("Sin materia") && !txt_materia5.getText().equals("Sin materia") && !txt_materia6.getText().equals("Sin materia") && !txt_materia7.getText().equals("Sin materia") && !txt_materia8.getText().equals("Sin materia") && !txt_optativa1.getText().equals("Sin materia") && !txt_optativa2.getText().equals("Sin materia")) {
            double sumapromfin, promfin;
            sumapromfin = Double.parseDouble(txt_m1final.getText()) + Double.parseDouble(txt_m2final.getText()) + Double.parseDouble(txt_m3final.getText()) + Double.parseDouble(txt_m4final.getText()) + Double.parseDouble(txt_m5final.getText()) + Double.parseDouble(txt_m6final.getText()) + Double.parseDouble(txt_m7final.getText()) + Double.parseDouble(txt_m8final.getText()) + Double.parseDouble(txt_o1final.getText()) + Double.parseDouble(txt_o2final.getText());
            promfin = sumapromfin / 10;
            txt_promsem.setText(String.valueOf(promfin));
            guardarPromedio(String.valueOf(promfin));
        } else {
            JOptionPane.showMessageDialog(this, "Número de materias incorrectas");
        }

    }//GEN-LAST:event_btn_promedioActionPerformed


    private void txt_promsemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_promsemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_promsemActionPerformed

    private void txt_idActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_idActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_idActionPerformed

    public void guardarPromedio(String prom) {

        String id,promsem,nocuenta, materia1, m1parcial1, m1parcial2, m1parcial3, m1final, materia2, m2parcial1, m2parcial2, m2parcial3, m2final, materia3, m3parcial1, m3parcial2, m3parcial3, m3final, materia4, m4parcial1, m4parcial2, m4parcial3, m4final, materia5, m5parcial1, m5parcial2, m5parcial3, m5final, materia6, m6parcial1, m6parcial2, m6parcial3, m6final, materia7, m7parcial1, m7parcial2, m7parcial3, m7final, materia8, m8parcial1, m8parcial2, m8parcial3, m8final, optativa1, o1parcial1, o1parcial2, o1parcial3, o1final, optativa2, o2parcial1, o2parcial2, o2parcial3, o2final;

        nocuenta= txt_nocuenta.getText()+"_"+cb_semestre.getSelectedItem();
        
        promsem= prom;
        
        id = txt_id.getText();
        
        materia1 = txt_materia1.getText();
        m1parcial1 = txt_m1parcial1.getText();
        m1parcial2 = txt_m1parcial2.getText();
        m1parcial3 = txt_m1parcial3.getText();
        m1final = txt_m1final.getText();

        materia2 = txt_materia2.getText();
        m2parcial1 = txt_m2parcial1.getText();
        m2parcial2 = txt_m2parcial2.getText();
        m2parcial3 = txt_m2parcial3.getText();
        m2final = txt_m2final.getText();

        materia3 = txt_materia3.getText();
        m3parcial1 = txt_m3parcial1.getText();
        m3parcial2 = txt_m3parcial2.getText();
        m3parcial3 = txt_m3parcial3.getText();
        m3final = txt_m3final.getText();

        materia4 = txt_materia4.getText();
        m4parcial1 = txt_m4parcial1.getText();
        m4parcial2 = txt_m4parcial2.getText();
        m4parcial3 = txt_m4parcial3.getText();
        m4final = txt_m4final.getText();

        materia5 = txt_materia5.getText();
        m5parcial1 = txt_m5parcial1.getText();
        m5parcial2 = txt_m5parcial2.getText();
        m5parcial3 = txt_m5parcial3.getText();
        m5final = txt_m5final.getText();

        materia6 = txt_materia6.getText();
        m6parcial1 = txt_m6parcial1.getText();
        m6parcial2 = txt_m6parcial2.getText();
        m6parcial3 = txt_m6parcial3.getText();
        m6final = txt_m6final.getText();

        materia7 = txt_materia7.getText();
        m7parcial1 = txt_m7parcial1.getText();
        m7parcial2 = txt_m7parcial2.getText();
        m7parcial3 = txt_m7parcial3.getText();
        m7final = txt_m7final.getText();

        materia8 = txt_materia8.getText();
        m8parcial1 = txt_m8parcial1.getText();
        m8parcial2 = txt_m8parcial2.getText();
        m8parcial3 = txt_m8parcial3.getText();
        m8final = txt_m8final.getText();

        optativa1 = txt_optativa1.getText();
        o1parcial1 = txt_o1parcial1.getText();
        o1parcial2 = txt_o1parcial2.getText();
        o1parcial3 = txt_o1parcial3.getText();
        o1final = txt_o1final.getText();

        optativa2 = txt_optativa2.getText();
        o2parcial1 = txt_o2parcial1.getText();
        o2parcial2 = txt_o2parcial2.getText();
        o2parcial3 = txt_o2parcial3.getText();
        o2final = txt_o2final.getText();
        
        

        if (txt_nocuenta.getText().equals("") || cb_semestre.getSelectedItem().equals("Seleccione")) {
            JOptionPane.showMessageDialog(this, "Introduzca el numero de cuenta y el semestre");

        } else {
            try {
                String url = "jdbc:mysql://localhost:3306/universidadpro";
                String usuario = "root";
                String contraseña = "";
                
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con = DriverManager.getConnection(url,usuario,contraseña);
                if(con != null){
                      System.out.println("Se ha establecido una conexión a la base de datos " +  
                                       "\n " + url ); 
                    
                }
                stmt = con.createStatement();
                stmt.executeUpdate("UPDATE IGNORE materias SET idmaterias = '"+ id +"', nocuenta = '"+ nocuenta +"', materia1 = '"+ materia1 +"', m1parcial1 = '"+ m1parcial1 +"', m1parcial2= '"+ m1parcial2 +"', m1parcial3 = '"+ m1parcial3 +"', promat1 = '"+ m1final +"', materia2 = '"+ materia2 +"', m2parcial1 = '"+ m2parcial1 +"', m2parcial2= '"+ m2parcial2 +"', m2parcial3 = '"+ m2parcial3 +"', promat2 = '"+ m2final +"', materia3 = '"+ materia3 +"', m3parcial1 = '"+ m3parcial1 +"', m3parcial2= '"+ m3parcial2 +"', m3parcial3 = '"+ m3parcial3 +"', promat3 = '"+ m3final +"', materia4 = '"+ materia4 +"', m4parcial1 = '"+ m4parcial1 +"', m4parcial2= '"+ m4parcial2 +"', m4parcial3 = '"+ m4parcial3 +"', promat4 = '"+ m4final +"', materia5 = '"+ materia5 +"', m5parcial1 = '"+ m5parcial1 +"', m5parcial2= '"+ m5parcial2 +"', m5parcial3 = '"+ m5parcial3 +"', promat5 = '"+ m5final +"', materia6 = '"+ materia6 +"', m6parcial1 = '"+ m6parcial1 +"', m6parcial2= '"+ m6parcial2 +"', m6parcial3 = '"+ m6parcial3 +"', promat6 = '"+ m6final +"', materia7 = '"+ materia7 +"', m7parcial1 = '"+ m7parcial1 +"', m7parcial2= '"+ m7parcial2 +"', m7parcial3 = '"+ m7parcial3 +"', promat7 = '"+ m7final +"', materia8 = '"+ materia8 +"', m8parcial1 = '"+ m8parcial1 +"', m8parcial2= '"+ m8parcial2 +"', m8parcial3 = '"+ m8parcial3 +"', promat8 = '"+ m8final +"', optativa1 = '"+ optativa1 +"', o1parcial1 = '"+ o1parcial1 +"', o1parcial2= '"+ o1parcial2 +"', o1parcial3 = '"+ o1parcial3 +"', proop1 = '"+ o1final +"', optativa2 = '"+ optativa2 +"', o2parcial1 = '"+ o2parcial1 +"', o2parcial2= '"+ o2parcial2 +"', o2parcial3 = '"+ o2parcial3 +"', proop2 = '"+ o2final +"',promsem = '"+ promsem +"' ");
                   System.out.println("Los valores han sido Actualizados"); 
               
                   
                   
            } catch( SQLException e ) { 
                      e.printStackTrace(); 
                  } catch (ClassNotFoundException ex) { 
            Logger.getLogger(Calificaciones.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(Calificaciones.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Calificaciones.class.getName()).log(Level.SEVERE, null, ex);
        } 
  
              finally { 
                  if ( con != null ) { 
                      try    { 
                          con.close(); 
                          stmt.close(); 
                      } catch( Exception e ) { 
                          System.out.println( e.getMessage()); 
                      } 
                  } 
     }
     javax.swing.JOptionPane.showMessageDialog(this,"Actualizado correctamente!","AVISO!",javax.swing.JOptionPane.INFORMATION_MESSAGE);
     } 
        
        

    }

    public void consulta() {
        String cap = "";
        ResultSet rs = null;
        var2 = var;
        String sql2 = "Select idmaterias,nocuenta, materia1, m1parcial1, m1parcial2, m1parcial3,promat1, materia2, m2parcial1, m2parcial2, m2parcial3, promat2, materia3, m3parcial1, m3parcial2, m3parcial3, promat3, materia4, m4parcial1, m4parcial2, m4parcial3, promat4, materia5, m5parcial1, m5parcial2, m5parcial3, promat5, materia6, m6parcial1, m6parcial2, m6parcial3, promat6, materia7, m7parcial1, m7parcial2, m7parcial3, promat7, materia8, m8parcial1, m8parcial2, m8parcial3, promat8, optativa1, o1parcial1, o1parcial2, o1parcial3, proop1, optativa2, o2parcial1, o2parcial2, o2parcial3, proop2,promsem FROM materias where nocuenta = '" + var2 + "'";

        try {

            String url = "jdbc:mysql://localhost:3306/universidadpro";
            String usuario = "root";
            String contraseña = "";

            Class.forName("com.mysql.jdbc.Driver").newInstance();

            con = DriverManager.getConnection(url, usuario, contraseña);

            if (con != null) {
                System.out.println("Se ha establecido una conexión a la base de datos "
                        + "\n " + url);
            }

            stmt = con.createStatement();
            rs = stmt.executeQuery(sql2);

            int i = 1;
            while (rs.next()) {
                
                String pr = rs.getString("promsem");
                
                String id = rs.getString("idmaterias");
                
                String materia1 = rs.getString("materia1");
                String m1parcial1 = rs.getString("m1parcial1");
                String m1parcial2 = rs.getString("m1parcial2");
                String m1parcial3 = rs.getString("m1parcial3");
                String promat1 = rs.getString("promat1");

                String materia2 = rs.getString("materia2");
                String m2parcial1 = rs.getString("m2parcial1");
                String m2parcial2 = rs.getString("m2parcial2");
                String m2parcial3 = rs.getString("m3parcial3");
                String promat2 = rs.getString("promat2");

                String materia3 = rs.getString("materia3");
                String m3parcial1 = rs.getString("m3parcial1");
                String m3parcial2 = rs.getString("m3parcial2");
                String m3parcial3 = rs.getString("m3parcial3");
                String promat3 = rs.getString("promat3");

                String materia4 = rs.getString("materia4");
                String m4parcial1 = rs.getString("m4parcial1");
                String m4parcial2 = rs.getString("m4parcial2");
                String m4parcial3 = rs.getString("m4parcial3");
                String promat4 = rs.getString("promat4");

                String materia5 = rs.getString("materia5");
                String m5parcial1 = rs.getString("m5parcial1");
                String m5parcial2 = rs.getString("m5parcial2");
                String m5parcial3 = rs.getString("m5parcial3");
                String promat5 = rs.getString("promat5");

                String materia6 = rs.getString("materia6");
                String m6parcial1 = rs.getString("m6parcial1");
                String m6parcial2 = rs.getString("m6parcial2");
                String m6parcial3 = rs.getString("m6parcial3");
                String promat6 = rs.getString("promat6");

                String materia7 = rs.getString("materia7");
                String m7parcial1 = rs.getString("m7parcial1");
                String m7parcial2 = rs.getString("m7parcial2");
                String m7parcial3 = rs.getString("m7parcial3");
                String promat7 = rs.getString("promat7");

                String materia8 = rs.getString("materia8");
                String m8parcial1 = rs.getString("m8parcial1");
                String m8parcial2 = rs.getString("m8parcial2");
                String m8parcial3 = rs.getString("m8parcial3");
                String promat8 = rs.getString("promat8");

                String optativa1 = rs.getString("optativa1");
                String o1parcial1 = rs.getString("o1parcial1");
                String o1parcial2 = rs.getString("o1parcial2");
                String o1parcial3 = rs.getString("o1parcial3");
                String proop1 = rs.getString("proop1");

                String optativa2 = rs.getString("optativa2");
                String o2parcial1 = rs.getString("o2parcial1");
                String o2parcial2 = rs.getString("o2parcial2");
                String o2parcial3 = rs.getString("o2parcial3");
                String proop2 = rs.getString("proop2");

                System.out.println("Sitio Web " + (i++) + ":\n"
                        + materia1 + "\n"
                        + materia2 + "\n"
                        + materia3 + "\n"
                        + materia4 + "\n"
                        + materia5 + "\n"
                        + materia6 + "\n"
                        + materia7 + "\n"
                        + materia8 + "\n"
                        + optativa1 + "\n"
                        + optativa2 + "\n"
                );
                
                txt_id.setText(id);
                txt_promsem.setText(pr);
                
                
                txt_materia1.setText(materia1);
                txt_m1parcial1.setText(m1parcial1);
                txt_m1parcial2.setText(m1parcial2);
                txt_m1parcial3.setText(m1parcial3);
                txt_m1final.setText(promat1);

                txt_materia2.setText(materia2);
                txt_m2parcial1.setText(m2parcial1);
                txt_m2parcial2.setText(m2parcial2);
                txt_m2parcial3.setText(m2parcial3);
                txt_m2final.setText(promat2);

                txt_materia3.setText(materia3);
                txt_m3parcial1.setText(m3parcial1);
                txt_m3parcial2.setText(m3parcial2);
                txt_m3parcial3.setText(m3parcial3);
                txt_m3final.setText(promat3);

                txt_materia4.setText(materia4);
                txt_m4parcial1.setText(m4parcial1);
                txt_m4parcial2.setText(m4parcial2);
                txt_m4parcial3.setText(m4parcial3);
                txt_m4final.setText(promat4);

                txt_materia5.setText(materia5);
                txt_m5parcial1.setText(m5parcial1);
                txt_m5parcial2.setText(m5parcial2);
                txt_m5parcial3.setText(m5parcial3);
                txt_m5final.setText(promat5);

                txt_materia6.setText(materia6);
                txt_m6parcial1.setText(m6parcial1);
                txt_m6parcial2.setText(m6parcial2);
                txt_m6parcial3.setText(m6parcial3);
                txt_m6final.setText(promat6);

                txt_materia7.setText(materia7);
                txt_m7parcial1.setText(m7parcial1);
                txt_m7parcial2.setText(m7parcial2);
                txt_m7parcial3.setText(m7parcial3);
                txt_m7final.setText(promat7);

                txt_materia8.setText(materia8);
                txt_m8parcial1.setText(m8parcial1);
                txt_m8parcial2.setText(m8parcial2);
                txt_m8parcial3.setText(m8parcial3);
                txt_m8final.setText(promat8);

                txt_optativa1.setText(optativa1);
                txt_o1parcial1.setText(o1parcial1);
                txt_o1parcial2.setText(o1parcial2);
                txt_o1parcial3.setText(o1parcial3);
                txt_o1final.setText(proop1);

                txt_optativa2.setText(optativa2);
                txt_o2parcial1.setText(o2parcial1);
                txt_o2parcial2.setText(o2parcial2);
                txt_o2parcial3.setText(o2parcial3);
                txt_o2final.setText(proop2);
                //txt_id.setText(id);
                //txt_nombre.setText(inom);
                //txt_correo.setText(idom);
                //txt_telefono.setText(itel);
                //txt_usuario.setText(inick);
                //txt_contra.setText(ipass);
                //txt_tipouser.setText(itipo);

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            Logger.getLogger(Calificaciones.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Calificaciones.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Calificaciones.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                    ex.printStackTrace();
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                    ex.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Calificaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Calificaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Calificaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Calificaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Calificaciones().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_consultar;
    private javax.swing.JButton btn_promedio;
    private javax.swing.JButton btn_regresar;
    private javax.swing.JComboBox<String> cb_semestre;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lb_final_mat;
    private javax.swing.JLabel lb_id;
    private javax.swing.JLabel lb_materia1;
    private javax.swing.JLabel lb_materia2;
    private javax.swing.JLabel lb_materia3;
    private javax.swing.JLabel lb_materia4;
    private javax.swing.JLabel lb_materia5;
    private javax.swing.JLabel lb_materia6;
    private javax.swing.JLabel lb_materia7;
    private javax.swing.JLabel lb_materia8;
    private javax.swing.JLabel lb_nocuenta;
    private javax.swing.JLabel lb_optativa1;
    private javax.swing.JLabel lb_optativa2;
    private javax.swing.JLabel lb_parcial1_mat;
    private javax.swing.JLabel lb_parcial2_mat;
    private javax.swing.JLabel lb_parcial3_mat;
    private javax.swing.JLabel lb_promsem;
    private javax.swing.JTextField txt_id;
    private javax.swing.JTextField txt_m1final;
    private javax.swing.JTextField txt_m1parcial1;
    private javax.swing.JTextField txt_m1parcial2;
    private javax.swing.JTextField txt_m1parcial3;
    private javax.swing.JTextField txt_m2final;
    private javax.swing.JTextField txt_m2parcial1;
    private javax.swing.JTextField txt_m2parcial2;
    private javax.swing.JTextField txt_m2parcial3;
    private javax.swing.JTextField txt_m3final;
    private javax.swing.JTextField txt_m3parcial1;
    private javax.swing.JTextField txt_m3parcial2;
    private javax.swing.JTextField txt_m3parcial3;
    private javax.swing.JTextField txt_m4final;
    private javax.swing.JTextField txt_m4parcial1;
    private javax.swing.JTextField txt_m4parcial2;
    private javax.swing.JTextField txt_m4parcial3;
    private javax.swing.JTextField txt_m5final;
    private javax.swing.JTextField txt_m5parcial1;
    private javax.swing.JTextField txt_m5parcial2;
    private javax.swing.JTextField txt_m5parcial3;
    private javax.swing.JTextField txt_m6final;
    private javax.swing.JTextField txt_m6parcial1;
    private javax.swing.JTextField txt_m6parcial2;
    private javax.swing.JTextField txt_m6parcial3;
    private javax.swing.JTextField txt_m7final;
    private javax.swing.JTextField txt_m7parcial1;
    private javax.swing.JTextField txt_m7parcial2;
    private javax.swing.JTextField txt_m7parcial3;
    private javax.swing.JTextField txt_m8final;
    private javax.swing.JTextField txt_m8parcial1;
    private javax.swing.JTextField txt_m8parcial2;
    private javax.swing.JTextField txt_m8parcial3;
    private javax.swing.JTextField txt_materia1;
    private javax.swing.JTextField txt_materia2;
    private javax.swing.JTextField txt_materia3;
    private javax.swing.JTextField txt_materia4;
    private javax.swing.JTextField txt_materia5;
    private javax.swing.JTextField txt_materia6;
    private javax.swing.JTextField txt_materia7;
    private javax.swing.JTextField txt_materia8;
    private javax.swing.JTextField txt_nocuenta;
    private javax.swing.JTextField txt_o1final;
    private javax.swing.JTextField txt_o1parcial1;
    private javax.swing.JTextField txt_o1parcial2;
    private javax.swing.JTextField txt_o1parcial3;
    private javax.swing.JTextField txt_o2final;
    private javax.swing.JTextField txt_o2parcial1;
    private javax.swing.JTextField txt_o2parcial2;
    private javax.swing.JTextField txt_o2parcial3;
    private javax.swing.JTextField txt_optativa1;
    private javax.swing.JTextField txt_optativa2;
    private javax.swing.JTextField txt_promsem;
    // End of variables declaration//GEN-END:variables
}
