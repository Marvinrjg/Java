
import java.awt.Image;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Panel extends javax.swing.JFrame {

    Connection con = null;
    Statement stmt = null;

    public Panel() {
        initComponents();
        this.setTitle("Panel");
   setIconImage(new ImageIcon(getClass().getResource("/imagenes/1473.png")).getImage());
   
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        cb_semestre = new javax.swing.JComboBox<>();
        lb_semestre = new javax.swing.JLabel();
        lb_materia1 = new javax.swing.JLabel();
        lb_materia2 = new javax.swing.JLabel();
        lb_materia4 = new javax.swing.JLabel();
        lb_materia3 = new javax.swing.JLabel();
        lb_materia5 = new javax.swing.JLabel();
        lb_optativa1 = new javax.swing.JLabel();
        lb_optativa2 = new javax.swing.JLabel();
        txt_materia1 = new javax.swing.JTextField();
        txt_materia2 = new javax.swing.JTextField();
        txt_materia3 = new javax.swing.JTextField();
        txt_materia4 = new javax.swing.JTextField();
        txt_materia5 = new javax.swing.JTextField();
        txt_optativa1 = new javax.swing.JTextField();
        txt_optativa2 = new javax.swing.JTextField();
        btn_vermat = new javax.swing.JButton();
        lb_materia6 = new javax.swing.JLabel();
        lb_materia7 = new javax.swing.JLabel();
        txt_materia6 = new javax.swing.JTextField();
        txt_materia7 = new javax.swing.JTextField();
        lb_materia8 = new javax.swing.JLabel();
        txt_materia8 = new javax.swing.JTextField();
        btn_guardar = new javax.swing.JButton();
        btn_limpiar = new javax.swing.JButton();
        lb_nocuenta = new javax.swing.JLabel();
        txt_nocuenta = new javax.swing.JTextField();
        btn_consultarm = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/depositphotos_107284674-stock-photo-crumpled-paper-texture-for-background.jpg"))); // NOI18N
        jLabel2.setText("jLabel2");

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/depositphotos_107284674-stock-photo-crumpled-paper-texture-for-background.jpg"))); // NOI18N
        jLabel3.setText("jLabel3");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel1.setText("REGISTRO");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 20, -1, -1));

        cb_semestre.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        cb_semestre.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione", "Semestre 1", "Semestre 2", "Semestre 3", "Semestre 4", "Semestre 5", "Semestre 6", "Semestre 7", "Semestre 8", "Semestre 9" }));
        cb_semestre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_semestreActionPerformed(evt);
            }
        });
        getContentPane().add(cb_semestre, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 90, 140, -1));

        lb_semestre.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_semestre.setText("Semestre:");
        getContentPane().add(lb_semestre, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 90, -1, -1));

        lb_materia1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia1.setText("Materia 1:");
        getContentPane().add(lb_materia1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 130, -1, -1));

        lb_materia2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia2.setText("Materia 2:");
        getContentPane().add(lb_materia2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 170, -1, -1));

        lb_materia4.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia4.setText("Materia 4:");
        getContentPane().add(lb_materia4, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 250, -1, -1));

        lb_materia3.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia3.setText("Materia 3:");
        getContentPane().add(lb_materia3, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 210, -1, -1));

        lb_materia5.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia5.setText("Materia 5:");
        getContentPane().add(lb_materia5, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 290, -1, -1));

        lb_optativa1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_optativa1.setText("Optativa 1:");
        getContentPane().add(lb_optativa1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 450, -1, -1));

        lb_optativa2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_optativa2.setText("Optativa 2:");
        getContentPane().add(lb_optativa2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 490, -1, -1));

        txt_materia1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txt_materia1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_materia1ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_materia1, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 130, 360, -1));

        txt_materia2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txt_materia2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_materia2ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_materia2, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 170, 360, -1));

        txt_materia3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txt_materia3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_materia3ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_materia3, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 210, 360, -1));

        txt_materia4.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txt_materia4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_materia4ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_materia4, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 250, 360, -1));

        txt_materia5.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txt_materia5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_materia5ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_materia5, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 290, 360, -1));

        txt_optativa1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txt_optativa1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_optativa1ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_optativa1, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 450, 360, -1));

        txt_optativa2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txt_optativa2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_optativa2ActionPerformed(evt);
            }
        });
        getContentPane().add(txt_optativa2, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 490, 360, -1));

        btn_vermat.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        btn_vermat.setText("Ver Materias");
        btn_vermat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_vermatActionPerformed(evt);
            }
        });
        getContentPane().add(btn_vermat, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 80, -1, -1));

        lb_materia6.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia6.setText("Materia 6:");
        getContentPane().add(lb_materia6, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 330, -1, -1));

        lb_materia7.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia7.setText("Materia 7:");
        getContentPane().add(lb_materia7, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 370, -1, -1));

        txt_materia6.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        getContentPane().add(txt_materia6, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 330, 360, -1));

        txt_materia7.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        getContentPane().add(txt_materia7, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 370, 360, -1));

        lb_materia8.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_materia8.setText("Materia 8:");
        getContentPane().add(lb_materia8, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 410, -1, -1));

        txt_materia8.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        getContentPane().add(txt_materia8, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 410, 360, -1));

        btn_guardar.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        btn_guardar.setText("Guardar");
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });
        getContentPane().add(btn_guardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 160, 160, 90));

        btn_limpiar.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        btn_limpiar.setText("Limpiar");
        btn_limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_limpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btn_limpiar, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 270, 160, 90));

        lb_nocuenta.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb_nocuenta.setText("No.Cuenta:");
        getContentPane().add(lb_nocuenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 50, -1, -1));

        txt_nocuenta.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        getContentPane().add(txt_nocuenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 50, 140, -1));

        btn_consultarm.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        btn_consultarm.setText("Consultar Calificaciónes");
        btn_consultarm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_consultarmActionPerformed(evt);
            }
        });
        getContentPane().add(btn_consultarm, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 410, 260, 100));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/depositphotos_107284674-stock-photo-crumpled-paper-texture-for-background.jpg"))); // NOI18N
        jLabel4.setText("jLabel4");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 890, 560));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_materia1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_materia1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_materia1ActionPerformed

    private void txt_materia2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_materia2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_materia2ActionPerformed

    private void txt_materia3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_materia3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_materia3ActionPerformed

    private void txt_materia4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_materia4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_materia4ActionPerformed

    private void txt_materia5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_materia5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_materia5ActionPerformed

    private void txt_optativa1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_optativa1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_optativa1ActionPerformed

    private void txt_optativa2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_optativa2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_optativa2ActionPerformed

    private void cb_semestreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_semestreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb_semestreActionPerformed

    private void btn_vermatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_vermatActionPerformed
        if(cb_semestre.getSelectedItem().equals("Seleccione")){
            JOptionPane.showMessageDialog(this, "Selecciona un semestre");
        }
        
        if (cb_semestre.getSelectedIndex() > 0) {
            txt_materia1.setText("Álgebra");
            txt_materia2.setText("Cálculo Diferencial e Integral");
            txt_materia3.setText("Computadoras y Programación");
            txt_materia4.setText("Geometria Alnalítica");
            txt_materia5.setText("Introduccion a la Ingenieria en Computación");
             txt_materia6.setText("Sin materia");
            txt_materia7.setText("Sin materia");
            txt_materia8.setText("Sin materia");
            txt_optativa1.setText("Sin materia");
            txt_optativa2.setText("Sin materia");
            //txt_materia6.setText("");
        }
        if (cb_semestre.getSelectedIndex() > 1) {
            txt_materia1.setText("Programación Orientada a Objetos");
            txt_materia2.setText("Cálculo Vetorial");
            txt_materia3.setText("Álgebra Lineal");
            txt_materia4.setText("Administración Contabilidad y Costos");
            txt_materia5.setText("Comunicación Oral y Escrita");
             txt_materia6.setText("Sin materia");
            txt_materia7.setText("Sin materia");
            txt_materia8.setText("Sin materia");
            txt_optativa1.setText("Sin materia");
            txt_optativa2.setText("Sin materia");
            //txt_materia6.setText("");
        }
        if (cb_semestre.getSelectedIndex() > 2) {
            txt_materia1.setText("Electricidad y Magnetismo");
            txt_materia2.setText("Estructura de Datos");
            txt_materia3.setText("Introducción a la Economía");
            txt_materia4.setText("Métodos Numéricos");
            txt_materia5.setText("Ecuaciones Diferenciales");
             txt_materia6.setText("Sin materia");
            txt_materia7.setText("Sin materia");
            txt_materia8.setText("Sin materia");
            txt_optativa1.setText("Sin materia");
            txt_optativa2.setText("Sin materia");
            //txt_materia6.setText("");
        }
        if (cb_semestre.getSelectedIndex() > 3) {
            txt_materia1.setText("Análisis de Circuitos Eléctricos");
            txt_materia2.setText("Probabilidad y Estadística");
            txt_materia3.setText("Estructuras Discretas");
            txt_materia4.setText("Ingeniería Económica");
            txt_materia5.setText("Investigación de Operaciónes y Sistemas");
             txt_materia6.setText("Sin materia");
            txt_materia7.setText("Sin materia");
            txt_materia8.setText("Sin materia");
            txt_optativa1.setText("Sin materia");
            txt_optativa2.setText("Sin materia");
            //txt_materia6.setText("");
        }
        if (cb_semestre.getSelectedIndex() > 4) {
            txt_materia1.setText("Dispositivos Electrónicos");
            txt_materia2.setText("Lenguajes Formales y Autómatas");
            txt_materia3.setText("Programación de Sistemas");
            txt_materia4.setText("Diseño y Análisis de Algoritmos");
            txt_materia5.setText("Medición e Instrumentación");
             txt_materia6.setText("Sin materia");
            txt_materia7.setText("Sin materia");
            txt_materia8.setText("Sin materia");
            txt_optativa1.setText("Sin materia");
            txt_optativa2.setText("Sin materia");
            //txt_materia6.setText("");
        }
        if (cb_semestre.getSelectedIndex() > 5) {
            txt_materia1.setText("Compiladores");
            txt_materia2.setText("Sistemas Operativos");
            txt_materia3.setText("Ingenieria de Software I");
            txt_materia4.setText("Sistemas Comunicaciónes");
            txt_materia5.setText("Diseño Lógico");
            txt_materia6.setText("Calidad");
             txt_materia7.setText("Sin materia");
            txt_materia8.setText("Sin materia");
            txt_optativa1.setText("Sin materia");
            txt_optativa2.setText("Sin materia");
        }
        if (cb_semestre.getSelectedIndex() > 6) {
            txt_materia1.setText("Dinámica de Sistemas Físicos");
            txt_materia2.setText("Bases de Datos I");
            txt_materia3.setText("Redes de Computadoras I");
            txt_materia4.setText("Seguridad Informática");
            txt_materia5.setText("Diseño de Sistemas Digitales");
             txt_materia6.setText("Sin materia");
            txt_materia7.setText("Sin materia");
            txt_materia8.setText("Sin materia");
            txt_optativa1.setText("Sin materia");
            txt_optativa2.setText("Sin materia");
            //txt_materia6.setText("");
        }
        if (cb_semestre.getSelectedIndex() > 7) {
            txt_materia1.setText("Adquisición de Datos");
            txt_materia2.setText("Bases de Datos II");
            txt_materia3.setText("Sistemas de Información");
            txt_materia4.setText("Microprocesador y Microcontroladores");
            txt_materia5.setText("Organización y Administración de Centros de Computo");
            txt_materia6.setText("Sistemas de Control");
            txt_materia7.setText("Graficación por Computadora");
            txt_materia8.setText("Sistemas Expertos");
            txt_optativa1.setText("Procesamiento Digital de Imágenes");
            txt_optativa2.setText("Diseño Asistido por Computadora");
        }
        if (cb_semestre.getSelectedIndex() > 8) {
            txt_materia1.setText("Administración Sistemas Multiusua");
            txt_materia2.setText("Administración de Tecnologías de la Información");
            txt_materia3.setText("Seminario de Ingenieria en Computación");
            txt_materia4.setText("Temas Especiales de Bases de Datos");
            txt_materia5.setText("Temas Especiales de Redes");
            txt_materia6.setText("Bioingenieria");
            txt_materia7.setText("Inteligencia Artificial");
            txt_materia8.setText("Temas Especiales de Computación");
            txt_optativa1.setText("Proyecto Escuela Industria");
            txt_optativa2.setText("Gestión de Redes de Computadoras");
            //txt_materia6.setText("Hablidiades Directivas");
            //txt_materia6.setText("Sistemas Expertos");
            //txt_materia6.setText("Robotica");
            //txt_materia6.setText("Visualización");
        }

    }//GEN-LAST:event_btn_vermatActionPerformed

    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed

        String nocuenta, materia1, materia2, materia3, materia4, materia5, materia6, materia7, materia8, op1, op2;

        nocuenta = txt_nocuenta.getText() + "_" + (cb_semestre.getSelectedItem());
        materia1 = txt_materia1.getText();
        materia2 = txt_materia2.getText();
        materia3 = txt_materia3.getText();
        materia4 = txt_materia4.getText();
        materia5 = txt_materia5.getText();
        materia6 = txt_materia6.getText();
        materia7 = txt_materia7.getText();
        materia8 = txt_materia8.getText();
        op1 = txt_optativa1.getText();
        op2 = txt_optativa2.getText();

        if (txt_nocuenta.getText().equals("") || cb_semestre.getSelectedItem().equals("Seleccione")) {
            JOptionPane.showMessageDialog(this, "Completa el numero de cuenta y el semestre");
        } else {
            try {
                String url = "jdbc:mysql://localhost:3306/universidadpro";
                String usuario = "root";
                String contraseña = "";

                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con = DriverManager.getConnection(url, usuario, contraseña);
                if (con != null) {
                    System.out.println("Se ha establecido una conexión a la base de datos "
                            + "\n " + url);
                }
                stmt = con.createStatement();
                stmt.executeUpdate("INSERT INTO materias VALUES ('" + 0 + "','" + nocuenta + "','" + materia1 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + materia2 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + materia3 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + materia4 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + materia5 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + materia6 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + materia7 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + materia8 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + op1 + "','" + 0 + "','" + 0 + "','" + 0 + "','"+0+"','" + op2 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','"+ 0 +"')");
                System.out.println("Los valores han sido agregados a la base de datos ");

            } catch (InstantiationException ex) {
                Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (con != null) {
                    try {
                        con.close();
                        stmt.close();
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }

                }
            }

            javax.swing.JOptionPane.showMessageDialog(this, "Registro exitoso! \n", "AVISO!", javax.swing.JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btn_guardarActionPerformed
    }

    private void btn_limpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_limpiarActionPerformed
        txt_materia1.setText("");
        txt_materia2.setText("");
        txt_materia3.setText("");
        txt_materia4.setText("");
        txt_materia5.setText("");
        txt_materia6.setText("");
        txt_materia7.setText("");
        txt_materia8.setText("");
        txt_optativa1.setText("");
        txt_optativa2.setText("");
        txt_nocuenta.setText("");

        JOptionPane.showMessageDialog(this, "Puedes colocar tus propias materias");
    }//GEN-LAST:event_btn_limpiarActionPerformed

    private void btn_consultarmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_consultarmActionPerformed
        Calificaciones cl = new Calificaciones();
        cl.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_consultarmActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Panel().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_consultarm;
    private javax.swing.JButton btn_guardar;
    private javax.swing.JButton btn_limpiar;
    private javax.swing.JButton btn_vermat;
    private javax.swing.JComboBox<String> cb_semestre;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel lb_materia1;
    private javax.swing.JLabel lb_materia2;
    private javax.swing.JLabel lb_materia3;
    private javax.swing.JLabel lb_materia4;
    private javax.swing.JLabel lb_materia5;
    private javax.swing.JLabel lb_materia6;
    private javax.swing.JLabel lb_materia7;
    private javax.swing.JLabel lb_materia8;
    private javax.swing.JLabel lb_nocuenta;
    private javax.swing.JLabel lb_optativa1;
    private javax.swing.JLabel lb_optativa2;
    private javax.swing.JLabel lb_semestre;
    private javax.swing.JTextField txt_materia1;
    private javax.swing.JTextField txt_materia2;
    private javax.swing.JTextField txt_materia3;
    private javax.swing.JTextField txt_materia4;
    private javax.swing.JTextField txt_materia5;
    private javax.swing.JTextField txt_materia6;
    private javax.swing.JTextField txt_materia7;
    private javax.swing.JTextField txt_materia8;
    private javax.swing.JTextField txt_nocuenta;
    private javax.swing.JTextField txt_optativa1;
    private javax.swing.JTextField txt_optativa2;
    // End of variables declaration//GEN-END:variables
}
